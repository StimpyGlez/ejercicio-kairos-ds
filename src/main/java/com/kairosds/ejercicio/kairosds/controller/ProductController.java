package com.kairosds.ejercicio.kairosds.controller;

import com.kairosds.ejercicio.kairosds.model.Product;
import com.kairosds.ejercicio.kairosds.service.ProductService;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
import java.util.Optional;

@RestController
public class ProductController {

    @Autowired
    private ProductService productService;

    @ApiOperation(value = "Obtiene el listado de Todos los productos")
    @RequestMapping(value = "/products", method = RequestMethod.GET)
    public ResponseEntity listAllProduct() {
        List<Product> listProducts = productService.listAllProduct();
        return new ResponseEntity(listProducts, HttpStatus.OK);
    }

    @ApiOperation(value = "Obtiene un producto con base en su Product ID")
    @RequestMapping(value = "/product/{productId}", method = RequestMethod.GET)
    public ResponseEntity getProductById(@PathVariable("productId")  long productId) {
        Optional<Product> product = productService.getProductById(productId);
        if ( product.isPresent() ) {
            return new ResponseEntity(product, HttpStatus.OK);
        }else {
            return new ResponseEntity("No existe producto con el ID específicado", HttpStatus.NOT_FOUND);
        }
    }


    @ApiOperation(value = "Obtiene una lista de productos con base en la marca y valida que el header tenga el valor 78965088 con parametro ID_CLIENT_SESSION")
    @RequestMapping(value = "/product/marca/{marca}", method = RequestMethod.GET)
    public ResponseEntity getProductByMarca(@RequestHeader("ID_CLIENT_SESSION") String idClientSession, @PathVariable("marca") String marca) {

        if ( idClientSession != null && idClientSession.equals("78965088")) {
            List<Product> productList = productService.getProductByMarca(marca);
            return new ResponseEntity(productList, HttpStatus.OK);
        }else{
            return new ResponseEntity("Sesión no valida", HttpStatus.FORBIDDEN);
        }
    }

    @ApiOperation(value = "Agrega un nuevo productos")
    @RequestMapping(value = "/create", method = RequestMethod.POST)
    public ResponseEntity create(@Valid @RequestBody Product product) {
        Product productCreated = productService.create(product);
        return new ResponseEntity(productCreated, HttpStatus.CREATED);
    }

    @ApiOperation(value = "Actualiza un producto existente")
    @RequestMapping(value = "/update/{productId}", method = RequestMethod.PUT)
    public ResponseEntity update(@Valid @RequestBody Product product, @PathVariable("productId")  long productId) {
        Optional<Product> productExist = productService.getProductById(productId);
        if ( productExist.isPresent() ){
            productExist.get().setDescription(product.getDescription());
            productExist.get().setPrice(product.getPrice());
            productExist.get().setMarca(product.getMarca());
            Product productUpdatable = productService.update(productExist.get());
            return new ResponseEntity(productUpdatable, HttpStatus.OK);
        }else{
            return new ResponseEntity("No existe producto a actualizar", HttpStatus.NOT_FOUND);
        }

    }

}
