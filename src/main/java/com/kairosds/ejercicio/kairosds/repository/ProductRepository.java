package com.kairosds.ejercicio.kairosds.repository;

import com.kairosds.ejercicio.kairosds.model.Product;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface ProductRepository extends JpaRepository<Product, Long> {

    public List<Product> findAllByMarca(String marca);

}
