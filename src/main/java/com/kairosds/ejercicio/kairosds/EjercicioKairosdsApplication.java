package com.kairosds.ejercicio.kairosds;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class EjercicioKairosdsApplication {

	public static void main(String[] args) {
		SpringApplication.run(EjercicioKairosdsApplication.class, args);
	}

}
