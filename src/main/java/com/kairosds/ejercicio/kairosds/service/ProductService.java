package com.kairosds.ejercicio.kairosds.service;

import com.kairosds.ejercicio.kairosds.model.Product;
import com.kairosds.ejercicio.kairosds.repository.ProductRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class ProductService {

    @Autowired
    ProductRepository repository;

    public List<Product> listAllProduct(){
        return repository.findAll();
    }

    public Optional<Product> getProductById(Long productId){
        return repository.findById(productId);
    }

    public Product create(Product product){
        return repository.save(product);
    }

    public Product update(Product product){
        return repository.save(product);
    }

    public List<Product> getProductByMarca(String marca){
        return repository.findAllByMarca(marca);
    }

}
