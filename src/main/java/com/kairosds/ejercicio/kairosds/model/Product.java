package com.kairosds.ejercicio.kairosds.model;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Data
@Entity
@Table(name = "PRODUCTS")
public class Product {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "PRODUCT_ID")
    private Long productId;

    @ApiModelProperty(value = "Descripción del producto", required = true, example = "Tennis Bota")
    @Column(name = "DESCRIPTION")
    private String description;

    @ApiModelProperty(value = "Marca del producto", required = true, example = "Nike")
    @Column(name = "MARCA")
    private String marca;

    @ApiModelProperty(value = "Costo del producto", required = true, example = "1550.85")
    @Column(name = "PRICE")
    private Double price;


}
